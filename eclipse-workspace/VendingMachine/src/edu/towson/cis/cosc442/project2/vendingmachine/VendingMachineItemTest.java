package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class VendingMachineItemTest {
	
	VendingMachineItem cookies,soda,bar;
	
	@Before
	public void setUp() throws Exception {
// sets up the test cases
	    cookies = new VendingMachineItem("cookies", 1.25);
	    soda = new VendingMachineItem("soda", 1.25);
	    bar = new VendingMachineItem("bar", 1.25);
	}
	@After
	public void tearDown() throws Exception {

		cookies = null;
		soda = null;
		bar= null;
	}
	
	/** tests VendingMachineItem Constructor for a price < 0 */
	@Test(expected = VendingMachineException.class)
	public void testVendingMachineItemNonZeroPrice() {
		
		VendingMachineItem nonZeroPrice = new VendingMachineItem("chips", -1.0);
	}
	/** tests VendingMachineItem Constructor for empty cases */
	@Test(expected = VendingMachineException.class)
	public void testVendingMachineItemEmpty() {
			
		VendingMachineItem empty = new VendingMachineItem(null, 2.5);

	}
	
	/** tests String getName() */
	@Test
	public void getName() {
		
		assertEquals("cookies", cookies.getName());
		assertEquals("soda", soda.getName());
		assertEquals("bar", bar.getName());


	}
	
	/** tests VendingMachineItem Constructor for an empty item */
	@Test
	public void getPrice() {
		
		assertEquals(1.25, cookies.getPrice(), 0);
		assertEquals(1.25, soda.getPrice(), 0);
		assertEquals(1.25, bar.getPrice(), 0);
	}

}
