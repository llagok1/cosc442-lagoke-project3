package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class VendingMachineTest {
	VendingMachine vending;

	/** Declaring necessary test objects for {@link Rectangle} */
	

	/**
	 * Initializes the necessary test objects for the test cases to use.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp()throws Exception{
		//sets up the test cases
	 vending = new VendingMachine();

		
	}
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception{
	}
	@Test
	public void testAddItem() {
		VendingMachineItem chips = new VendingMachineItem("chips", 1.25);
			 vending.addItem(chips, "A");
		         
		        assertEquals("This item is added", vending.getItem("A"), chips);
		         
		    }
	@Test
	public void testRemoveItem() {
		//object declaraction of slot a
        VendingMachineItem chips = new VendingMachineItem("chips", 1.25);
        //adds the item into slot a
        vending.addItem(chips, "A");
        //gets the item from slot a and then removes it.
        assertEquals("this item is removed",vending.getItem("A"), vending.removeItem("A"));
        
	}
	@Test
	public void testInsertMoney() {
    int amount=1;
    vending.insertMoney(1.25);
	assertEquals( vending.insertMoney(amount),vending.getItem("A"));
	
	
}
	@Test
	public void testGetBalance() {
		// object declaration for the get method
		vending.getBalance();
		//adds to the expected value of the balance
		assertEquals("my balance is", vending.getBalance()+1.25);
		
	}
	@Test
	public void testMakePurchace() {
		vending.makePurchase("a");
		if("a"<1.25)
		{
			
		}
	
		
	}
	@SuppressWarnings("deprecation")
	public void testGetChange() {
		vending.returnChange();
		assertEquals(" your exact change is", vending.returnChange(),3.0);
	}
	
}

	


