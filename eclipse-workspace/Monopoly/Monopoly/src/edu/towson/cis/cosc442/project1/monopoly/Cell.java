package edu.towson.cis.cosc442.project1.monopoly;

public abstract class Cell implements IOwnable {
	private String name;
	protected Player theowner;
	private boolean available = true;

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Player getTheowner() {
		return theowner;
	}
	
	@Override
	public int getPrice() {
		return 0;
	}

	public boolean isAvailable() {
		return available;
	}
	
	public abstract Boolean playAction(String msg);

	@Override
	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	void setName(String name) {
		this.name = name;
	}

	@Override
	public void setTheOwner(Player owner) {
		this.theowner = owner;
	}
    
    @Override
	public String toString() {
        return name;
    }
}
